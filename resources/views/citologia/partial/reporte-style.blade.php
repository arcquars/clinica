<style>
    @page {
        header: page-header;
        footer: page-footer;

        margin-header: 13mm;
        margin-top: 4.5cm;
    }

    body {
        font-family: 'Times New Roman', 'Sansita Swashed', sans-serif;
    }

    .h1-cito{
        text-align: center;
        color: #012035;
        font-size: 16px;
        margin: 0;
        padding: 0;
    }
    .h2-cito{
        text-align: center;
        color: #012035;
        font-size: 12px;
        margin: 0;
        padding: 0;
    }

    .h4-cito-1{
        color: #053D62;
        font-size: 18px;
        margin: 0;
        padding: 0;
    }

    .h4-cito{
        color: #053D62;
        font-size: 14px;
        margin: 0;
        padding: 0;
    }
    .td-p-datos{
        font-size: 12px;
        color: #001781;
    }

    .p-dato {
        font-size: 16px;
        color: #012035;
    }

    .p-datos {
        font-size: 12px;
        color: #012035;
    }

    .p-datos-12 {
        /*font-family: 'Sansita Swashed', cursive;*/
        font-family: 'Times New Roman', cursive;
        font-size: 11px;
        color: #012035;
    }

    .p-datos-13 {
        font-size: 16px;
        color: #012035;
    }

    .p-datos-12-1 {
        /*font-family: 'Sansita Swashed', cursive;*/
        font-family: 'Times New Roman', cursive;
        font-size: 11px;
        color: #012035;
        line-height: 8px;
    }

    .t-extcompatible tr td{
        text-align: center;
        border: 1px solid;
    }

    .t-column-title{
        color: #012035;
        font-size: 10px;
        font-weight: bold;

    }

    .div-campo{
        padding: 2px;
        text-align: justify;
        text-justify: inter-word;
    }

    .div-campo h1 {
        font-size: 18px !important;
    }

    .div-campo h2 {
        font-size: 16px !important;
    }

    .div-campo h3 {
        font-size: 14px !important;
    }

    .div-campo h4 {
        font-size: 12px !important;
    }

    .div-campo p{
        font-size: 12px !important;
    }

    .div-campo ul li{
        font-size: 11px;
    }

    .div-campo ol li{
        font-size: 11px;
    }

    .clinica-table{
        border-collapse: collapse;
    }

    .clinica-table, .clinica-table tr td{
        border: 1px solid black;
    }

    .cito-estudio-p{
        font-size: 11px;
    }

    .cito-estudio-tr{
        font-size: 9px;
    }

    .div-observacion p {
        font-size: 12px;
    }

    .div-observacion h1 {
        font-size: 14px;
    }

    .div-observacion h2 {
        font-size: 12px;
    }

    .div-observacion h3 {
        font-size: 11px;
    }

    .div-observacion h4 {
        font-size: 10px;
    }

    .t_images_4_p{
        font-size: 8px;
    }
</style>
