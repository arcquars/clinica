<nav class="navbar navbar-expand-lg " color-on-scroll="500">
    <div class="container-fluid">
        <a class="navbar-brand" href="#"> {{ $navName }} </a>
        <button href="" class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-bar burger-lines"></span>
            <span class="navbar-toggler-bar burger-lines"></span>
            <span class="navbar-toggler-bar burger-lines"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-end" id="navigation">
            <ul class="nav navbar-nav mr-auto">
                <li class="nav-item">
                    <a href="#" class="nav-link" data-toggle="dropdown">
                        <span class="d-lg-none">Tablero</span>
                    </a>
                </li>
            </ul>
            <ul class="navbar-nav   d-flex align-items-center">
                <li class="nav-link">
                    <a href="#" class="text-success" data-toggle="modal" data-target="#textPreModal"><i class="fas fa-atlas"></i> </a>
                </li>
                <li class="nav-item">
                    <span class="no-icon">
                        @php
                        $roles = auth()->user()->roles;
                        $rolesStr = '';
                        foreach ($roles as $role){
                            $rolesStr .= $role->name.', ';
                        }
                        @endphp
                        {{auth()->user()->name}} <span style="font-size: .75rem">({{strtoupper(substr($rolesStr, 0, -2))}}) |</span>
                    </span>
                </li>
{{--                <li class="nav-item">--}}
{{--                    <a class="nav-link" href=" ">--}}
{{--                        <span class="no-icon">Cuenta</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
                <li class="nav-item">
                    <form id="logout-form" action="{{ route('logout') }}" method="POST">
                        @csrf
                        <a class="text-danger" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Cerrar sesión</a>
                    </form>
                </li>
            </ul>
        </div>
    </div>
</nav>