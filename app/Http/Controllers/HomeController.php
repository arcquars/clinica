<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

class HomeController extends Controller
{
    public $layout = 'layouts.dash';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $dateF = Carbon::now();
//        $dateI = Carbon::now()->subDays(30);
        $dateI = Carbon::now()->firstOfMonth();
        return view('home', compact('dateI', 'dateF'));
    }
}
