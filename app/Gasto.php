<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gasto extends Model
{
    protected $table = 'gastos';

    protected $fillable = [
        'fecha',
        'detalle',
        'gasto'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }
}
